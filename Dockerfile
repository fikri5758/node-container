FROM node:latest
RUN mkdir /opt/app
COPY ./express-hello-world /opt/app
WORKDIR /opt/app
RUN npm install
RUN node index.js
